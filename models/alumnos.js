import conexion from "./conexion.js";

var alumnosDb = {}
alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve, reject) => {
        // Consulta
        let sqlConsulta = "Insert into alumnos set ?"
        conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                console.log("Surgio un Error ", err.message);
                reject(err);
            }else{
                const alumno ={
                    id:res.id
                }
                resolve(alumno);
            
            }
        });
    });

}

alumnosDb.mostrarTodos = function mostrarTodos(){
    return new Promise((resolve, rejects) => {
        
        let sqlConsulta = "Select * from alumnos"
        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                console.log("Surgio un error ", err);
                rejects(err);
            }else{
                resolve(res);
            }
        });
    });
}

export default alumnosDb;