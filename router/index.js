import express from 'express';

import json from 'body-parser';

import alumnosDb from '../models/alumnos.js';

export const router = express.Router();

router.get('/',(req,res)=>{

    res.render('index',{titulo:"Mis Practicas JS",nombre:"Osuna Bueno Merari Jazel"})

})

router.get('/tabla',(req,res)=>{

    // Parametros

    const params = {
        numero:req.query.numero
    }

    res.render('tabla',params);
})

router.post('/tabla',(req,res)=>{

    // Parametros

    const params = {
        numero:req.body.numero
    }

    res.render('tabla',params);
})

router.get('/cotizacion',(req,res)=>{

    // Parametros

    const params = {
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazos: req.query.plazos,
    }

    res.render('cotizacion',params);
})

router.post('/cotizacion',(req,res)=>{

    // Parametros

    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos,
    }

    res.render('cotizacion',params);
})

let rows; 

router.get('/alumnos', async (req,res)=>{
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows})
});

let params;

router.post('/alumnos', async(req,res)=>{
    try{
        params={
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domnicilio:req.body.domnicilio,
            sexo:req.body.sexo,
            especialidad:req.body.especialidad
        }
        const res = await alumnosDb.insertar(params);
    }catch(error){
        console.error(error)
        res.status(400).send("Sucedio un error: " + error)
    }

    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
})

export default {router}
